// Conn wraps a net.Conn with read/write methods for binary data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package binaryutil

import (
	"encoding/binary"
	"io"
	"net"
	"time"
)

// Conn is the interface encapsulating both the net.Conn and BinaryReadWriter interfaces.
type Conn interface {
	net.Conn
	ByteOrderer
	BasicReader
	BasicWriter
}

// ConnReader is a subset of the Conn interface, only supporting methods for reading data.
type ConnReader interface {
	io.Reader
	ByteOrderer
	BasicReader
	SetReadDeadline(t time.Time) error // SetReadDeadline sets the deadline for future Read calls and any currently-blocked Read call. A zero value for t means Read will not time out.
	LocalAddr() net.Addr               // LocalAddr returns the local network address.
	RemoteAddr() net.Addr              // RemoteAddr returns the remote network address.
}

// ConnWriter is a subset of the Conn interface, only supporting methods for writing data.
type ConnWriter interface {
	io.Writer
	ByteOrderer
	BasicWriter
	SetWriteDeadline(t time.Time) error // SetWriteDeadline sets the deadline for future Write calls and any currently-blocked Write call. Even if write times out, it may return n > 0, indicating that some of the data was successfully written. A zero value for t means Write will not time out.
	LocalAddr() net.Addr                // LocalAddr returns the local network address.
	RemoteAddr() net.Addr               // RemoteAddr returns the remote network address.
}

// ConnReadWriteCloser is the interface encapsulating both the ConnReadWriteCloser and BinaryReadWriter interfaces.
type ConnReadWriteCloser interface {
	net.Conn
	ByteOrderer
	BasicReader
	BasicWriter
	CloseRead() error  // CloseRead shuts down the reading side of the connection. Most callers should just use Close.
	CloseWrite() error // CloseWrite shuts down the writing side of the connection. Most callers should just use Close.
}

// binaryConn wraps a net.Conn and implements the Conn interface.
type binaryConn struct {
	*ReadWriteCloser
	c net.Conn // The underlying connection
}

// binaryConnReadWriteCloser wraps a ConnReadWriteCloser and implements the ConnReadWriteCloser interface.
type binaryConnReadWriteCloser struct {
	*binaryConn
}

/////////////////////////////////////////////////////////////////////////
// binaryConn functions
/////////////////////////////////////////////////////////////////////////

// NewConn wraps the given net.Conn and binary.ByteOrder as a Conn.
func NewConn(c net.Conn, e binary.ByteOrder) Conn {
	return newBinaryConn(c, e)
}

// newBinaryConn wraps the given net.Conn and binary.ByteOrder as a binaryConn.
func newBinaryConn(c net.Conn, e binary.ByteOrder) *binaryConn {
	return &binaryConn{
		NewReadWriteCloser(c, e),
		c,
	}
}

// LocalAddr returns the local network address.
func (c *binaryConn) LocalAddr() net.Addr {
	return c.c.LocalAddr()
}

// RemoteAddr returns the remote network address.
func (c *binaryConn) RemoteAddr() net.Addr {
	return c.c.RemoteAddr()
}

// SetDeadline sets the read and write deadlines associated with the connection. It is equivalent to calling both SetReadDeadline and SetWriteDeadline. A deadline is an absolute time after which I/O operations fail with a timeout (see type Error) instead of blocking. The deadline applies to all future I/O, not just the immediately following call to Read or Write. An idle timeout can be implemented by repeatedly extending the deadline after successful Read or Write calls. A zero value for t means I/O operations will not time out.
func (c *binaryConn) SetDeadline(t time.Time) error {
	return c.c.SetDeadline(t)
}

// SetReadDeadline sets the deadline for future Read calls. A zero value for t means Read will not time out.
func (c *binaryConn) SetReadDeadline(t time.Time) error {
	return c.c.SetReadDeadline(t)
}

// SetWriteDeadline sets the deadline for future Write calls. Even if write times out, it may return n > 0, indicating that some of the data was successfully written. A zero value for t means Write will not time out.
func (c *binaryConn) SetWriteDeadline(t time.Time) error {
	return c.c.SetWriteDeadline(t)
}

/////////////////////////////////////////////////////////////////////////
// binaryConnReadWriteCloser functions
/////////////////////////////////////////////////////////////////////////

// NewConnReadWriteCloser wraps the given ConnReadWriteCloser and binary.ByteOrder as a ConnReadWriteCloser.
func NewConnReadWriteCloser(c ConnReadWriteCloser, e binary.ByteOrder) ConnReadWriteCloser {
	return &binaryConnReadWriteCloser{newBinaryConn(c, e)}
}

// CloseRead shuts down the reading side of the connection. Most callers should just use Close.
func (c *binaryConnReadWriteCloser) CloseRead() error {
	if rwc, ok := c.c.(ConnReadWriteCloser); ok {
		return rwc.CloseRead()
	}
	return nil
}

// CloseWrite shuts down the writing side of the connection. Most callers should just use Close.
func (c *binaryConnReadWriteCloser) CloseWrite() error {
	if rwc, ok := c.c.(ConnReadWriteCloser); ok {
		return rwc.CloseWrite()
	}
	return nil
}
