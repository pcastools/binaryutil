// Binaryutil provides objects and interfaces to help working with binary data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package binaryutil

import (
	"encoding/binary"
	"io"
)

// ByteOrderer is the interface satisfied by the ByteOrder method.
type ByteOrderer interface {
	ByteOrder() binary.ByteOrder // ByteOrder returns the byte order used.
}

// BasicReader describes the interface for the basic methods to read binary data.
type BasicReader interface {
	io.ByteReader
	ReadUint8() (uint8, error)   // ReadUint8 reads a uint8.
	ReadInt8() (int8, error)     // ReadInt8 reads an int8.
	ReadUint16() (uint16, error) // ReadUint16 reads a uint16.
	ReadInt16() (int16, error)   // ReadInt16 reads an int16.
	ReadUint32() (uint32, error) // ReadUint32 reads a uint32.
	ReadInt32() (int32, error)   // ReadInt32 reads an int32.
	ReadUint64() (uint64, error) // ReadUint64 reads a uint64.
	ReadInt64() (int64, error)   // ReadInt64 reads an int64.
}

// BasicWriter describes the interface for the basic methods to write binary data.
type BasicWriter interface {
	io.ByteWriter
	WriteUint8(m uint8) error   // WriteUint8 writes a uint8.
	WriteInt8(m int8) error     // WriteInt8 writes an int8.
	WriteUint16(m uint16) error // WriteUint16 writes a uint16.
	WriteInt16(m int16) error   // WriteInt16 writes an int16.
	WriteUint32(m uint32) error // WriteUint32 writes a uint32.
	WriteInt32(m int32) error   // WriteInt32 writes an int32.
	WriteUint64(m uint64) error // WriteUint64 writes a uint64.
	WriteInt64(m int64) error   // WriteInt64 writes an int64.
}

// BinaryReader describes the interface for methods to read binary data.
type BinaryReader interface {
	io.Reader
	ByteOrderer
	BasicReader
}

// BinaryWriter describes the interface for methods to write binary data.
type BinaryWriter interface {
	io.Writer
	ByteOrderer
	BasicWriter
}

// BinaryReadWriter describes the interface for methods to read and write binary data.
type BinaryReadWriter interface {
	io.ReadWriter
	ByteOrderer
	BasicReader
	BasicWriter
}

// readByteFunc reads and returns a byte.
type readByteFunc func() (byte, error)

// baseReader wraps an io.Reader and implements BinaryReader.
type baseReader struct {
	r   io.Reader        // The underlying reader
	rb  readByteFunc     // The ReadByte implementation
	e   binary.ByteOrder // The byte order being used
	buf []byte           // A buffer of length 8
}

// Reader wraps an io.Reader and implements BinaryReader.
type Reader struct {
	*baseReader
}

// ReadCloser wraps an io.ReadCloser and implements BinaryReader.
type ReadCloser struct {
	*baseReader
}

// writeByteFunc writes a byte.
type writeByteFunc func(b byte) error

// baseWriter wraps an io.Writer and implements BinaryWriter.
type baseWriter struct {
	w   io.Writer        // The underlying writer
	wb  writeByteFunc    // The WriteByte implementation
	e   binary.ByteOrder // The byte order being used
	buf []byte           // A buffer of length 8
}

// Writer wraps an io.Writer and implements BinaryWriter.
type Writer struct {
	*baseWriter
}

// WriteCloser wraps an io.WriteCloser and implements BinaryWriter.
type WriteCloser struct {
	*baseWriter
}

// ReadWriter wraps an io.ReadWriter and implements BinaryReader and BinaryWriter.
type ReadWriter struct {
	*baseReader
	*baseWriter
}

// ReadWriteCloser wraps an io.ReadWriteCloser and implements BinaryReader and BinaryWriter.
type ReadWriteCloser struct {
	*baseReader
	*baseWriter
}

// maxEmptyReadAttempts is the number of consecutive empty reads before giving up.
const maxEmptyReadAttempts = 100

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// readExactly reads exactly n bytes from the io.Reader r into the slice p. The slice p must be of length at least n, otherwise an io.ErrShortBuffer error will be returned.
func readExactly(r io.Reader, n int, p []byte) error {
	if len(p) < n {
		return io.ErrShortBuffer
	}
	p = p[:n]
	i := 0
	for i < maxEmptyReadAttempts {
		if m, err := r.Read(p); err != nil {
			return err
		} else if m == n {
			return nil
		} else if m == 0 {
			i++
		} else {
			n -= m
			p = p[m:]
			i = 0
		}
	}
	return io.ErrNoProgress
}

/////////////////////////////////////////////////////////////////////////
// baseReader functions
/////////////////////////////////////////////////////////////////////////

// newBaseReader wraps the given io.Reader as a baseReader using the given byte order.
func newBaseReader(r io.Reader, e binary.ByteOrder) *baseReader {
	wrapped := &baseReader{
		r:   r,
		e:   e,
		buf: make([]byte, 8),
	}
	if br, ok := r.(io.ByteReader); ok {
		wrapped.rb = br.ReadByte
	} else {
		wrapped.rb = func() (byte, error) {
			return ReadByte(wrapped.r, wrapped.buf)
		}
	}
	return wrapped
}

// Read reads data from r.
func (r *baseReader) Read(b []byte) (int, error) {
	return r.r.Read(b)
}

// ReadByte reads a byte from r.
func (r *baseReader) ReadByte() (byte, error) {
	return r.rb()
}

// ReadUint8 reads a uint8 from r.
func (r *baseReader) ReadUint8() (uint8, error) {
	b, err := r.ReadByte()
	return uint8(b), err
}

// ReadInt8 reads an int8 from r.
func (r *baseReader) ReadInt8() (int8, error) {
	b, err := r.ReadByte()
	return int8(b), err
}

// ReadUint16 reads a uint16 from r.
func (r *baseReader) ReadUint16() (uint16, error) {
	return ReadUint16(r.r, r.e, r.buf)
}

// ReadInt16 reads an int16 from r.
func (r *baseReader) ReadInt16() (int16, error) {
	return ReadInt16(r.r, r.e, r.buf)
}

// ReadUint32 reads a uint32 from r.
func (r *baseReader) ReadUint32() (uint32, error) {
	return ReadUint32(r.r, r.e, r.buf)
}

// ReadInt32 reads an int32 from r.
func (r *baseReader) ReadInt32() (int32, error) {
	return ReadInt32(r.r, r.e, r.buf)
}

// ReadUint64 reads a uint64 from r.
func (r *baseReader) ReadUint64() (uint64, error) {
	return ReadUint64(r.r, r.e, r.buf)
}

// ReadInt64 reads an int64 from r.
func (r *baseReader) ReadInt64() (int64, error) {
	return ReadInt64(r.r, r.e, r.buf)
}

/////////////////////////////////////////////////////////////////////////
// Reader functions
/////////////////////////////////////////////////////////////////////////

// NewReader wraps the given io.Reader as a BinaryReader using the given byte order.
func NewReader(r io.Reader, e binary.ByteOrder) *Reader {
	return &Reader{newBaseReader(r, e)}
}

// ByteOrder returns the byte order used.
func (r *Reader) ByteOrder() binary.ByteOrder {
	return r.e
}

/////////////////////////////////////////////////////////////////////////
// ReadCloser functions
/////////////////////////////////////////////////////////////////////////

// NewReadCloser wraps the given io.ReadCloser as a BinaryReader using the given byte order.
func NewReadCloser(r io.ReadCloser, e binary.ByteOrder) *ReadCloser {
	return &ReadCloser{newBaseReader(r, e)}
}

// ByteOrder returns the byte order used.
func (r *ReadCloser) ByteOrder() binary.ByteOrder {
	return r.e
}

// Close closes the underlying reader.
func (r *ReadCloser) Close() error {
	if rc, ok := r.r.(io.Closer); ok {
		return rc.Close()
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// baseWriter functions
/////////////////////////////////////////////////////////////////////////

// newBaseWriter wraps the given io.Writer as a baseWriter using the given byte order.
func newBaseWriter(w io.Writer, e binary.ByteOrder) *baseWriter {
	wrapped := &baseWriter{
		w:   w,
		e:   e,
		buf: make([]byte, 8),
	}
	if bw, ok := w.(io.ByteWriter); ok {
		wrapped.wb = bw.WriteByte
	} else {
		wrapped.wb = func(b byte) error {
			return WriteByte(wrapped.w, b, wrapped.buf)
		}
	}
	return wrapped
}

// Write writes data to w.
func (w *baseWriter) Write(b []byte) (int, error) {
	return w.w.Write(b)
}

// WriteByte write the byte b to w.
func (w *baseWriter) WriteByte(b byte) error {
	return w.wb(b)
}

// WriteUint8 writes a uint8 to w
func (w *baseWriter) WriteUint8(m uint8) error {
	return w.WriteByte(byte(m))
}

// WriteInt8 writes an int8 to w.
func (w *baseWriter) WriteInt8(m int8) error {
	return w.WriteByte(byte(m))
}

// WriteUint16 writes a uint16 to w.
func (w *baseWriter) WriteUint16(m uint16) error {
	return WriteUint16(w.w, w.e, m, w.buf)
}

// WriteInt16 writes an int16 to w.
func (w *baseWriter) WriteInt16(m int16) error {
	return WriteInt16(w.w, w.e, m, w.buf)
}

// WriteUint32 writes a uint32 to w.
func (w *baseWriter) WriteUint32(m uint32) error {
	return WriteUint32(w.w, w.e, m, w.buf)
}

// WriteInt32 writes an int32 to w.
func (w *baseWriter) WriteInt32(m int32) error {
	return WriteInt32(w.w, w.e, m, w.buf)
}

// WriteUint64 writes a uint64 to w.
func (w *baseWriter) WriteUint64(m uint64) error {
	return WriteUint64(w.w, w.e, m, w.buf)
}

// WriteInt64 writes an int64 to w.
func (w *baseWriter) WriteInt64(m int64) error {
	return WriteInt64(w.w, w.e, m, w.buf)
}

/////////////////////////////////////////////////////////////////////////
// Writer functions
/////////////////////////////////////////////////////////////////////////

// NewWriter wraps the given io.Writer as a BinaryWriter using the given byte order.
func NewWriter(w io.Writer, e binary.ByteOrder) *Writer {
	return &Writer{newBaseWriter(w, e)}
}

// ByteOrder returns the byte order used.
func (w *Writer) ByteOrder() binary.ByteOrder {
	return w.e
}

/////////////////////////////////////////////////////////////////////////
// WriteCloser functions
/////////////////////////////////////////////////////////////////////////

// NewWriteCloser wraps the given io.WriteCloser as a BinaryWriter using the given byte order.
func NewWriteCloser(w io.WriteCloser, e binary.ByteOrder) *WriteCloser {
	return &WriteCloser{newBaseWriter(w, e)}
}

// ByteOrder returns the byte order used.
func (w *WriteCloser) ByteOrder() binary.ByteOrder {
	return w.e
}

// Close closes the underlying writer.
func (w *WriteCloser) Close() error {
	if wc, ok := w.w.(io.Closer); ok {
		return wc.Close()
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// ReadWriter functions
/////////////////////////////////////////////////////////////////////////

// NewReadWriter wraps the given io.ReadWriter as a BinaryReader and BinaryWriter using the given byte order.
func NewReadWriter(rw io.ReadWriter, e binary.ByteOrder) *ReadWriter {
	return &ReadWriter{
		newBaseReader(rw, e),
		newBaseWriter(rw, e),
	}
}

// ByteOrder returns the byte order used.
func (rw *ReadWriter) ByteOrder() binary.ByteOrder {
	return rw.baseReader.e
}

/////////////////////////////////////////////////////////////////////////
// ReadWriteCloser functions
/////////////////////////////////////////////////////////////////////////

// NewReadWriteCloser wraps the given io.ReadWriteCloser as a BinaryReader and BinaryWriter using the given byte order.
func NewReadWriteCloser(rw io.ReadWriteCloser, e binary.ByteOrder) *ReadWriteCloser {
	return &ReadWriteCloser{
		newBaseReader(rw, e),
		newBaseWriter(rw, e),
	}
}

// ByteOrder returns the byte order used.
func (rw *ReadWriteCloser) ByteOrder() binary.ByteOrder {
	return rw.baseReader.e
}

// Close closes the underlying writer.
func (rw *ReadWriteCloser) Close() error {
	if rwc, ok := rw.r.(io.Closer); ok {
		return rwc.Close()
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ReadByte reads a byte from the io.Reader r using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned. Note that this will NOT check to see if r satisfies the io.ByteReader interface.
func ReadByte(r io.Reader, p []byte) (byte, error) {
	if err := readExactly(r, 1, p); err != nil {
		return 0, err
	}
	return p[0], nil
}

// ReadUint8 reads a uint8 from the io.Reader r using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned.
func ReadUint8(r io.Reader, p []byte) (uint8, error) {
	n, err := ReadByte(r, p)
	return uint8(n), err
}

// ReadInt8 reads an int8 from the io.Reader r using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned.
func ReadInt8(r io.Reader, p []byte) (int8, error) {
	n, err := ReadByte(r, p)
	return int8(n), err
}

// ReadUint16 reads a uint16 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 2, otherwise an io.ErrShortBuffer error will be returned.
func ReadUint16(r io.Reader, e binary.ByteOrder, p []byte) (uint16, error) {
	if err := readExactly(r, 2, p); err != nil {
		return 0, err
	}
	return e.Uint16(p[:2]), nil
}

// ReadInt16 reads an int16 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 2, otherwise an io.ErrShortBuffer error will be returned.
func ReadInt16(r io.Reader, e binary.ByteOrder, p []byte) (int16, error) {
	n, err := ReadUint16(r, e, p)
	return int16(n), err
}

// ReadUint32 reads a uint32 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 4, otherwise an io.ErrShortBuffer error will be returned.
func ReadUint32(r io.Reader, e binary.ByteOrder, p []byte) (uint32, error) {
	if err := readExactly(r, 4, p); err != nil {
		return 0, err
	}
	return e.Uint32(p[:4]), nil
}

// ReadInt32 reads an int32 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 4, otherwise an io.ErrShortBuffer error will be returned.
func ReadInt32(r io.Reader, e binary.ByteOrder, p []byte) (int32, error) {
	n, err := ReadUint32(r, e, p)
	return int32(n), err
}

// ReadUint64 reads a uint64 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 8, otherwise an io.ErrShortBuffer error will be returned.
func ReadUint64(r io.Reader, e binary.ByteOrder, p []byte) (uint64, error) {
	if err := readExactly(r, 8, p); err != nil {
		return 0, err
	}
	return e.Uint64(p[:8]), nil
}

// ReadInt64 reads an int64 from the io.Reader r with byte order e, using the slice p for temporary storage. The slice p must be of length at least 8, otherwise an io.ErrShortBuffer error will be returned.
func ReadInt64(r io.Reader, e binary.ByteOrder, p []byte) (int64, error) {
	n, err := ReadUint64(r, e, p)
	return int64(n), err
}

// WriteByte write the byte b to the io.Writer w using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned. Note that this will NOT check to see if w satisfies the io.ByteWriter interface.
func WriteByte(w io.Writer, b byte, p []byte) error {
	if len(p) < 1 {
		return io.ErrShortBuffer
	}
	p[0] = b
	if n, err := w.Write(p[:1]); err != nil {
		return err
	} else if n < 1 {
		return io.ErrShortWrite
	}
	return nil
}

// WriteUint8 writes the uint8 m to the io.Writer w using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned.
func WriteUint8(w io.Writer, m uint8, p []byte) error {
	return WriteByte(w, byte(m), p)
}

// WriteInt8 writes the int8 m to the io.Writer w using the slice p for temporary storage. The slice p must be of length at least 1, otherwise an io.ErrShortBuffer error will be returned.
func WriteInt8(w io.Writer, m int8, p []byte) error {
	return WriteByte(w, byte(m), p)
}

// WriteUint16 writes the uint16 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 2, otherwise an io.ErrShortBuffer error will be returned.
func WriteUint16(w io.Writer, e binary.ByteOrder, m uint16, p []byte) error {
	if len(p) < 2 {
		return io.ErrShortBuffer
	}
	e.PutUint16(p[:2], m)
	if n, err := w.Write(p[:2]); err != nil {
		return err
	} else if n < 2 {
		return io.ErrShortWrite
	}
	return nil
}

// WriteInt16 writes the int16 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 2, otherwise an io.ErrShortBuffer error will be returned.
func WriteInt16(w io.Writer, e binary.ByteOrder, m int16, p []byte) error {
	return WriteUint16(w, e, uint16(m), p)
}

// WriteUint32 writes the uint32 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 4, otherwise an io.ErrShortBuffer error will be returned.
func WriteUint32(w io.Writer, e binary.ByteOrder, m uint32, p []byte) error {
	if len(p) < 4 {
		return io.ErrShortBuffer
	}
	e.PutUint32(p[:4], m)
	if n, err := w.Write(p[:4]); err != nil {
		return err
	} else if n < 4 {
		return io.ErrShortWrite
	}
	return nil
}

// WriteInt32 writes the int32 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 4, otherwise an io.ErrShortBuffer error will be returned.
func WriteInt32(w io.Writer, e binary.ByteOrder, m int32, p []byte) error {
	return WriteUint32(w, e, uint32(m), p)
}

// WriteUint64 writes the uint64 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 8, otherwise an io.ErrShortBuffer error will be returned.
func WriteUint64(w io.Writer, e binary.ByteOrder, m uint64, p []byte) error {
	if len(p) < 8 {
		return io.ErrShortBuffer
	}
	e.PutUint64(p[:8], m)
	if n, err := w.Write(p[:8]); err != nil {
		return err
	} else if n < 8 {
		return io.ErrShortWrite
	}
	return nil
}

// WriteInt64 writes the int64 m to the io.Writer w using the byte order e and the slice p for temporary storage. The slice p must be of length at least 8, otherwise an io.ErrShortBuffer error will be returned.
func WriteInt64(w io.Writer, e binary.ByteOrder, m int64, p []byte) error {
	return WriteUint64(w, e, uint64(m), p)
}
